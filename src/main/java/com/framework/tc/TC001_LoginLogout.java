package com.framework.tc;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LoginLogout";
		testDescription = "Login into leaftaps";
		testNodes = "Leads";
		author = "Gayathri";
		category = "smoke";
		dataSheetName = "TC001";
	}
	

	@Test(dataProvider="fetchData")
	public void login(String username, String password, String companyname, String firstname, String lastname) {
		
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCRM()
    	.clickLead()
		.clickCreatLead()
		.enterCname(companyname)
		.enterFname(firstname)
		.enterLname(lastname)
		.clickCreate();
		
		
		
		
		
		
		
		
		//.clickLogout();
		
		
		
//		LoginPage lp = new LoginPage();
//		lp.enterUsername("");
//		lp.enterPassword("");

	}


	private Object clickCRM() {
		// TODO Auto-generated method stub
		return null;
	}
}

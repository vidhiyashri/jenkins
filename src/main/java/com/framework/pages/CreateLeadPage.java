package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCname;
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFname;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLname;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleSubmit;
	//Enter company name
	public CreateLeadPage enterCname(String companyname) {
		clearAndType(eleCname, companyname);
		return this;
	}
	
	
	//Enter Firstname
	public CreateLeadPage enterFname(String firstname) {
		clearAndType(eleFname, firstname);
		return this;
	
	}
	
	//Enter Lastname
	public CreateLeadPage enterLname(String lastname) {
		clearAndType(eleLname, lastname);
		return this;
	
	}
	
	public ViewLeadPage clickCreate() {
		click(eleSubmit);
		return new ViewLeadPage();
	}
	
}
